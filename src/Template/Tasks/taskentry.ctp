<script>
    function ErrorDate() {
        alert("無効な日付です");
    }
    function ErrorContent() {
        alert("無効な内容です");
    }
</script>
<?php
if (isset($error)) {
    if ($error == 1) {
        echo '<script>ErrorDate()</script>';
    } else {
        echo '<script>ErrorContent()</script>';
    }
}
?>
<div class="leftBlock">
    <h1 class="fontChange">
        <?php
        echo $LecturesName[0]['name'];
        ?>
        に
        <br/>課題登録</h1>
</div>
<form method = "post" action="
<?php
$url = $this->Url->build(['action' => 'taskentry']);
$get = '?kougi=' . $kougi;
$act = $url . $get;
echo $act;
?>
      ">
    <div class="normal">
        <div class="input-size">
            <table>
                <tr> <th> <span class="vspace">内容</span> </th> <th> <input type="text" required name="taskname"> </th> </tr>
                <tr> <th> <span class="vspace">提出日</span> </th> <th> <select name="year"> 
                            <?php
                            $year = $y;
                            for ($i = 0; $i < 10; $i++) {
                                echo '<option value="' . $year . '">' . $year . '</option>';
                                $year++;
                            }
                            ?>
                        </select>
                        年
                        <select name="month">
                            <?php
                            for ($i = 1; $i <= 12; $i++) {
                                echo '<option value="' . $i . '"';
                                if ($i == $m) {
                                    echo 'selected';
                                }
                                echo '>';
                                echo $i . '</option>';
                            }
                            ?>
                        </select>
                        月
                        <select name="day">
                            <?php
                            for ($i = 1; $i <= 31; $i++) {
                                echo '<option value="' . $i . '"';
                                if ($i == $d) {
                                    echo 'selected';
                                }
                                echo '>';
                                echo $i . '</option>';
                            }
                            ?>
                        </select>
                        日
                    </th> </tr>
            </table> <br/>
        </div>
        <table>
            <tr> 
                <th>
                    <input type="submit" class ="large" value="登録">
                </th>
                <th>
                    <?php
                    echo $this->Html->link('戻る', [
                        'action' => 'tasklist' . $get
                    ]);
                    ?>
                </th>
            </tr>
        </table>
    </div>
</form>
