<div class="leftBlock">
    <h1 class="fontChange">講義一覧</h1>
</div>
<div class="rightBlock">
    <div class="buttonsize">
        <?php
        echo $this->Html->link('登録', [
            'action' => "ent"
        ]);
        ?>
    </div>
</div>

<div class="normal">
    <div class="input-large-size">

        <table>
            <?php
            $count = count($lec);
            for ($id = 0; $id < $count; $id++) {
                echo ' <tr> <th> <span class= "vspace">';
                echo $lec[$id]['name'];
                echo '</span> </th> <th>';
                $url = "tasklist?kougi=";
                $url .= $lec[$id]['id'];
                echo $this->Html->link('課題', [
                    'action' => $url
                ]);
                echo '</th> <th>';
                $del = "tasks?delete=";
                $del .= $lec[$id]['id'];
                $str = $lec[$id]['name'];
                echo $this->Html->link('削除', '#', [
                    'onclick' => "javascript:DeleteDialog(\"$str\", \"$del\")"
                ]);
                echo '</th> </tr>';
            }
            ?>
        </table>
    </div>
</div>