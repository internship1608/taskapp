<div class="leftBlock">
    <h1 class="fontChange">
        <?php
        echo $TaskName[0]['name'];
        ?>
        にファイル<br/>アップロード</h1>
</div>
<form   method="post" action="
<?php
$url = $this->Url->build(['action' => 'upload']);
$get1 = '?kougi=' . $kougi;
$get2 = '&kadai=' . $kadai;
echo $url . $get1 . $get2;
?>
        " enctype="multipart/form-data">
    <div class="normal">
        <div class="input-size">
            ファイルを選択<br/>
            <input type="file" required name="datafile"><br/>
        </div>
        <div class="input-size">
            <table>
                <tr>
                    <th>
                        <input type="submit" class="normal" value="アップロード">
                    </th>
                    <th>
                        <?php
                        echo $this->Html->link('戻る', [
                            'action' => 'tasklist' . $get1
                        ]);
                        ?>
                    </th>
                </tr>
            </table>
        </div>
    </div>

    <h2 class="fontChange">
        ファイル一覧
    </h2>
    <div class="normal">
        <div class="input-size">
            <table>
                <?php
                $count = count($FileList);
                for ($id = 0; $id < $count; $id++) {
                    echo '<tr> <th> <span class="vspace">';
                    echo $FileList[$id]['name'];
                    echo '</span> </th> <th>';
                    $del = 'upload' . $get1 . $get2;
                    $dow = $del . '&name=' . $FileList[$id]['id'];
                    $type = explode(".", $FileList[$id]['name']);
                    $dow .= '.' . $type[1];
                    echo $this->Html->link('ダウンロード', [
                        'action' => $dow
                    ]);
                    echo '</th> <th>';
                    $del .= '&delete=';
                    $del .= $FileList[$id]['id'];
                    $str = $FileList[$id]['name'];
                    echo $this->Html->link('削除', '#', [
                        'onclick' => "javascript:DeleteDialog(\"$str\", \"$del\")"
                    ]);
                    echo '</th> </tr>';
                }
                ?>
            </table>
        </div>
    </div>
</form>

