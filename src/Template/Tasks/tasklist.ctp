
<div class="leftBlock">
    <h1 class="fontChange">
        <?php
        echo $LecturesName[0]['name'];
        ?>
        の
        <br/>課題一覧</h1>
</div>
<div class="rightBlock">
    <div class="button">
        <?php
        $str = "taskentry?kougi=";
        $str .= $LecturesName[0]['id'];
        echo $this->Html->link('登録', [
            'action' => $str
        ]);
        ?>
    </div>
    <div class="button">
        <?php
        echo $this->Html->link('戻る', [
            'action' => "index"
        ]);
        ?>
    </div>
</div>
<div class="normal">
    <div class="input-large-size">
        <table>
            <?php
            $count = count($task);
            for ($id = 0; $id < $count; $id++) {
                echo '<tr> <th> <span class="vspace">';
                echo $task[$id]['name'];
                echo '</span> </th> <th> <span class="vspace">';
                echo date('Y/m/d', strtotime($task[$id]['date']));
                echo '</span> </th> <th> <span class="vspace">';
                if (isset($upfile)) {
                    echo $upfile[$id];
                } else {
                    echo 0;
                }
                echo '件';
                echo '</span> </th> <th>';
                $url = 'upload';
                $get = '?kougi=' . $task[$id]['lecture_id'];
                $url .= $get;
                $url .= '&kadai=';
                $url .= $task[$id]['id'];
                echo $this->Html->link('アップ', [
                    'action' => $url
                ]);
                echo '</th> <th>';
                $del = 'tasklist';
                $del .= $get;
                $del .= '&delete=';
                $del .= $task[$id]['id'];
                $str = $task[$id]['name'];
                echo $this->Html->link('削除', '#', [
                    'onclick' => "javascript:DeleteDialog(\"$str\", \"$del\")"
                ]);
                echo '</th> </tr>';
            }
            ?>
        </table>
    </div>
</div>

