<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

date_default_timezone_set('Asia/Tokyo');

class TasksController extends AppController {

    public function index() {
        $this->lectures = \Cake\ORM\TableRegistry::get("lectures");
        $del = $this->request->query('delete');
        if (isset($del)) {
            $this->tasks = \Cake\ORM\TableRegistry::get("tasks");
            $this->files = \Cake\ORM\TableRegistry::get("files");
            $dellecture = ['id' => $del];
            $deltask = ['lecture_id' => $del];
            $task = $this->tasks->find('all', ['conditions' => $deltask]);
            foreach ($task as $val) {
                $delfile = ['task_id' => $val['id']];
                $file = $this->files->find('all', [
                            'conditions' => $delfile
                        ])->first();
                if (isset($file)) {
                    $type = explode(".", $file['name']);
                    $delpath = WWW_ROOT . 'uploadfiles' . DS . $file['id'] . '.' . $type[1];
                    unlink($delpath);
                    $this->files->deleteAll($delfile);
                }
            }
            $this->tasks->deleteAll($deltask);
            $this->lectures->deleteAll($dellecture);
        }
        $lec = $this->lectures->find("all")->toArray();
        $this->set('lec', $lec);
    }

    public function ent() {
        $name = $this->request->data("lecturename");
        if (isset($name)) {
            if (preg_replace('/\s|　/', "", $name) != '') {
                $this->lectures = \Cake\ORM\TableRegistry::get("lectures");
                $entity = $this->lectures->newEntity(["name" => $name]);
                $this->lectures->save($entity);
                return $this->redirect(['action' => 'index']);
            } else {
                $error = 1;
                $this->set('error', $error);
            }
        }
    }

    public function taskentry() {
        $get = $this->request->query('kougi');
        if (isset($get)) {
            $this->lectures = \Cake\ORM\TableRegistry::get("lectures");
            $lec = $this->lectures->find('all', [
                        'conditions' => ['id' => $get]
                    ])->toArray();
            $this->set('LecturesName', $lec);
            $this->set('kougi', $get);
            $this->set('y', date('Y'));
            $this->set('m', date('m'));
            $this->set('d', date('d'));
        }

        $name = $this->request->data("taskname");
        $date = $this->request->data("year") . '-' . $this->request->data("month") . '-' . $this->request->data("day");
        if (isset($name)) {
            if (preg_replace('/\s|　/', "", $name) != '') {
                $this->tasks = \Cake\ORM\TableRegistry::get("tasks");
                $entity = $this->tasks->newEntity(["name" => $name, "date" => $date, "lecture_id" => $get]);

                if (date('Ymd', strtotime($date)) < date('Ymd')) {
                    $error = 1;
                    $this->set('error', $error);
                } else if ($this->tasks->save($entity)) {
                    return $this->redirect(['action' => 'tasklist?kougi=' . $get]);
                } else {
                    $error = 1;
                    $this->set('error', $error);
                }
            } else {
                $error = 2;
                $this->set('error', $error);
            }
        }
    }

    public function tasklist() {
        $get = $this->request->query('kougi');
        $del = $this->request->query('delete');
        if (isset($get)) {
            $this->lectures = \Cake\ORM\TableRegistry::get("lectures");
            $this->tasks = \Cake\ORM\TableRegistry::get("tasks");
            $this->files = \Cake\ORM\TableRegistry::get("files");

            if (isset($del)) {
                $deltask = ['id' => $del];
                $delfile = ['task_id' => $del];
                $file = $this->files->find('all', [
                            'conditions' => $delfile
                        ])->first();
                if (isset($file)) {
                    $type = explode(".", $file['name']);
                    $delpath = WWW_ROOT . 'uploadfiles' . DS . $file['id'] . '.' . $type[1];
                    unlink($delpath);
                    $this->files->deleteAll($delfile);
                }
                $this->tasks->deleteAll($deltask);
            }

            $lec = $this->lectures->find('all', [
                        'conditions' => ['id' => $get]
                    ])->toArray();
            $this->set('LecturesName', $lec);


            $TimeOut = $this->tasks->find('all', ['lecture_id' => $get]);
            foreach ($TimeOut as $val) {
                if (date('Ymd', strtotime($val['date'])) < date('Ymd')) {
                    $dtask = ['id' => $val['id']];
                    $dfile = ['task_id' => $val['id']];
                    $this->tasks->deleteAll($dtask);
                    $this->files->deleteAll($dfile);
                }
            }

            $task = $this->tasks->find('all', [
                        'conditions' => ['lecture_id' => $get],
                        'order' => 'date'
                    ])->toArray();
            $this->set('task', $task);

            $number = count($task);
            if ($number != 0) {
                for ($id = 0; $id < $number; $id++) {
                    $ans = $this->files->find('all', [
                        'conditions' => ['task_id' => $task[$id]['id']]
                    ]);
                    $upfile[] = $ans->count();
                }
                $this->set('upfile', $upfile);
            }
        }
    }

    public
            function upload() {
        $get = $this->request->query('kadai');
        $ret = $this->request->query('kougi');
        $del = $this->request->query('delete');
        $dow = $this->request->query('name');
        if (isset($get) && isset($ret)) {
            $this->tasks = \Cake\ORM\TableRegistry::get("tasks");
            $this->files = \Cake\ORM\TableRegistry::get("files");
            if (isset($del)) {
                $delfile = ['id' => $del];
                $file = $this->files->find('all', [
                            'conditions' => $delfile
                        ])->first();
                $type = explode(".", $file['name']);
                $delpath = WWW_ROOT . 'uploadfiles' . DS . $del . '.' . $type[1];
                unlink($delpath);
                $this->files->deleteAll($delfile);
            }
            if (isset($dow)) {
                $this->autoRender = false;
                $type = explode(".", $dow);
                $file = $this->files->find('all', [
                            'conditions' => ['id' => $type[0]]
                        ])->first();
                $path = WWW_ROOT . 'uploadfiles' . DS . $dow;
                header('Content-Type: application/force-download');
                header('Content-Length: ' . filesize($path));
                header('Content-Disposition: attachment; filename=' . $file['name']);
                readfile($path);
            }
            $task = $this->tasks->find('all', [
                        'conditions' => ['id' => $get]
                    ])->toArray();
            $files = $this->files->find('all', [
                        'conditions' => ['task_id' => $get]
                    ])->toArray();
            $this->set('TaskName', $task);
            $this->set('FileList', $files);
            $this->set('kadai', $get);
            $this->set('kougi', $ret);
        }

        $data = $this->request->data("datafile");
        if (!$data['error'] && isset($data)) {
            $this->files = \Cake\ORM\TableRegistry::get("files");
            $entity = $this->files->newEntity(["name" => $data['name'], "task_id" => $get]);
            $this->files->save($entity);

            $nameid = $this->files->find('all')->orderDesc('id')->first();
            $folder = WWW_ROOT . 'uploadfiles' . DS;
            $type = explode(".", $data['name']);
            $filename = $nameid['id'] . '.' . $type[1];
            move_uploaded_file($data['tmp_name'], $folder . $filename);
            return $this->redirect(['action' => 'tasklist?kougi=' . $ret]);
        }
    }

}
