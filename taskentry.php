<!DOCTYPE html>
<html lang ="ja">
    <head>
        <meta charset = "UTF-8" />
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>exercises_tree</title>
    </head>

    <body>
        <div class="leftBlock">
            <h1 class="fontChange">講義XXに<br/>課題登録</h1>
        </div>
        <form method = "post" action="tasklist.php">
            <div class="normal">
                <div class="input-size">
                    <table>
                        <tr> <th> <span class="vspace">内容</span> </th> <th> <input type="text" name="taskname"> </th> </tr>
                        <tr> <th> <span class="vspace">期限</span> </th> <th> <input type="date" name="tasklimit"> </th> </tr>
                    </table> <br/>
                </div>
                <input type="submit" value="登録">
            </div>
        </form>
    </body>
</html>