<!DOCTYPE html>
<html lang ="ja">
    <head>
        <meta charset = "UTF-8" />
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>exercises_tree</title>
    </head>

    <body>
        <div class="leftBlock">
            <h1 class="fontChange">
                講義
                <?php
                $kougi = filter_input(INPUT_GET, 'kougi');
                if (isset($kougi)) {
                    echo $kougi;
                }
                else {
                    echo '?';
                }
                ?>
                の
                <br/>課題一覧</h1>
        </div>
        <div class="rightBlock">
            <div class="buttonsize"><a class="button" href="taskentry.php">登録</a></div>
            <div class="buttonsize"><a class="button" href="index.php">戻る</a></div>
        </div>

        <div class="normal">
            <table>
                <tr> <th>
                        <span class="vspace">
                            <?php
                            $name = filter_input(INPUT_POST, 'taskname');
                            if (isset($name)) {
                                echo $name;
                            }
                            ?>
                        </span>
                    </th>
                    <th>
                        <span class="vspace">
                            <?php
                            $limit = filter_input(INPUT_POST, 'tasklimit');
                            if (isset($limit)) {
                                echo $limit;
                            }
                            ?>
                        </span>
                    </th>
                    <th>
                        <?php
                        if (isset($_FILES['datafile'])) {
                            $data = $_FILES['datafile'];
                        }

                        if (isset($data)) {
                            echo 1;
                        } else {
                            echo 0;
                        }
                        ?>
                        件
                    </th>
                    <th>
                        <a class="button" href="
                           <?php
                           $str = "upload.php?kougi=";
                           $str .= 1;
                           if (isset($name)) {
                               echo $str;
                           }
                           ?>
                           ">
                               <?php
                               if (isset($name)) {
                                   echo "アップ";
                               }
                               ?>
                        </a>
                    </th>
                </tr>
            </table>
        </div>
    </body>
</html>
