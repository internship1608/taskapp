<!DOCTYPE html>
<html lang ="ja">
    <head>
        <meta charset = "UTF-8" />
        <link rel="stylesheet" href="style.css" type="text/css">
        <title>exercises_tree</title>
    </head>

    <body>
        <div class="leftBlock">
            <h1 class="fontChange">講義XXにファイル<br/>アップロード</h1>
        </div>
        <form action="tasklist.php" method="post" enctype="multipart/form-data">
            <div class="normal">
                <div class="input-size">
                    ファイルを選択<br/>
                    <input type="file" name="datafile"><br/>
                </div>
                <input type="submit" value="アップロード">
            </div>
        </form>
    </body>
</html>
